![alt text](CoverImage/SRS_Image8__1_-1.png) 


# BAB I PENDAHULUAN

## 1.1  TUJUAN

Dokumen Software Requirement Specification (SRS) merupakan dokumen spesifikasi perangkat lunak untuk membangun "Sistem Konservasi Bersama Masyarakat". Dokumen ini dibangun untuk memudahkan dinas lingkungan hidup dan kehutanan dalam menginputkan data-data dari kegiatan-kegiatan konservasi yang sudah terjadi. Sehingga dokumen ini dapat dijadikan acuan teknis untuk membangun perangkat lunak "SISTEM KONSERVASI BERSAMA MASYARAKAT".

## 1.2  LINGKUP

Sistem Konservasi Bersama Masyarakat merupakan aplikasi yang kami bangun untuk mempermudah UPT KPH Tasik Besar Serkap dalam mendokumentasikan dan menyampaikan pesan tentang cara merawat hutan dengan secara singkat dan  lebih baik .dimana aplikasi ini dibuat untuk  memudahkan admin yang bertugas dalam menginputkan data-datanya.

## 1.3  Akronim, singkatan, definisi

## 1.4 Referensi

Referensi yang digunakan dalam pengembangan perangkat lunak ini adalah :
- http://hasantarmizi.blogspot.co.id/2017/04/pengertian-sublime-text.html
- IEEE. IEEE Std 830-1998 Praktik yang Direkomendasikan IEEE untuk Spesifikasi Persyaratan Perangkat Lunak. Masyarakat Komputer IEEE, 1998. 1.5 Ikhtisar
- SRSExample-webapp.pdf

## 1.5 Overview

Bab selanjutnya yaitu menjelaskan sistem yang diterapkan pada aplikasi. Menjelaskan gambaran umum dari aplikasi, sistem interface aplikasi dan alur sistemnya. Bab terakhir menjelaskan tentang setiap fungsi yang digunakan secara teknisnya. Pada bab 2 dan 3 merupakan deskripsi dari aplikasi yang akan diterapkan pada aplikasi yang dibuat.


# BAB II GAMBARAN UMUM

Pada saat ini  hutan yang menjadi paru-paru dunia sedang terancam ,yang mana tidak di perhatikan oleh lingkungan sekitar atau pun tidak mendapatkan perhatian khusus oleh masyarakat atau daerah setempat , padahal hutan mempunyai peran yang sangat penting untuk makhluk hidup baik manusia ,hewan dan tumbuhan . Pentingnya hutan bagi makhluk hidup tidak dapat diremehkan, dan pelestarian hutan dan pengelolaan yang berkelanjutan menjadi sangat penting untuk menjaga keberlanjutan kehidupan di Bumi. Hutan yang sehat dan lestari mendukung kehidupan kita secara langsung maupun tidak langsung, dan perlindungan serta pelestariannya adalah tugas yang harus diemban bersama oleh seluruh masyarakat global. 
Maka dari itu kami membuat sebuah sistem dimana bertujuan untuk mempermudah dalam mengkonservasi hutan dari perambahan yang dilakukan oleh oknum-oknum yang tidak bertanggung jawab.  Dimana pada sistem ini akan mudah dalam menginputkan data-data wilayah hutan yang dilindungi seperti input wilayah, input kasus perambahan dari tahun ke tahun, input konservasi yang telah dilakukan.dengan ini mempermudah admin dinas kehutanan menginputkan data-data yang menyangkut dalam pengelolaan hutan tersebut. Dan user yang view dari data-data yang telah di simpan  di dalam sistem tersebut.

Berikut akan kami jelaskan sistem software kami, admin fungsi utama yaitu :
- Menginputkan data & informasi
- Menginputkan kawasan konservasi
- Menginputkan kegiatan

Berikut ini fungsi user dalam bentuk tabel:
- View data & informasi
- View kawasan konservasi
- View kegiatan

## 2.1  Perspektif  Product

Sistem  Konservasi dan Perambahan  Hutan adalah sebuah sistem administrasi data yang diaplikasikan pada website. Terdapat 2 jenis yaitu admin, dan Kepala/ staf dinas kehutanan dan lingkungan hidup . Pengolahan data di kelola oleh admin  pada website dan kepala /staf hanya melihat data  dan laporan pada website.
Pada sistem konservasi dan perambahan hutan ini akan menampilkan data kasus dan konservasi hutan yang sudah diinputkan oleh admin.

### 2.1.1  Antarmuka Sistem

![alt text](2.1.1Antarmuka.png)

Sistem aplikasi Konservasi Bersama Masyarakat memiliki 3 user yaitu staf dinas, admin dan staf desa mempunyai fungsi yaitu melakukan view data dan informasi, view data desa, view kegiatan ,dan view kawasan konservasi.  Staf desa bisa menginputkan data kegiatan masing-masing desanya .Admin bertugas untuk mengelola data, supaya data bisa di akses oleh staf dinas dan staf desa.

### 2.1.2  Antarmuka pengguna
![alt text](Login_Page.png) 
![alt text](Dashboard.png) 
![alt text](Informasi_Desa.png)
![alt text](CRUD_Info_desa.png) 
![alt text](Kawasan.png) 
![alt text](CRUD_kawasan.png)
![alt text](Kegiatan.png) 
![alt text](CRUD_kegiatan.png)

### 2.1.3  Antarmuka perangkat keras
![alt text](2.1.3Antarmuka.png) 

Antarmuka perangkat keras yang digunakan untuk mengoperasikan Perangkat Lunak Manajemen Data yang diinputkan ke dalam sistem konservasi bersama masyarakat   antara lain :
- PC / Laptop Untuk menjalankan Aplikasi ini admin membutuhkan sebuah PC yang menggunakan OS Windows, Linux, atau MAC dan sudah terinstall browser .

### 2.1.4  Antarmuka perangkat lunak

Tidak ada

### 2.1.5  Antarmuka Komunikasi

Antarmuka komunikasi yang digunakan untuk mengoperasikan Perangkat Lunak Manajemen Data yang diinputkan dalam sistem konservasi bersama masyarakat antara lain :
- Kabel LAN
- WIFI
- Paket data

### 2.1.6  Batasan memori

Tidak ada

### 2.1.7 Operasi-operasi
Adapun operasi-operasi yang ada dalam sistem konservasi bersama masyarakat sebagai berikut : 

![alt text](2.1.7Operasi.png)

### 2.1.8   Kebutuhan adaptasi

Tidak ada

### 2.2  Spesifikasi Kebutuhan Fungsional
![alt text](2.2SpekFungsional.png)

### 2.2.1  Admin Login
Use Case Admin Login

![alt text](2.2.1.png)

Deskripsi Singkat Admin melakukan login dengan memasukan username password. Deskripsi Langkah-langkah

- Admin melakukan login dengan username dan password
- Sistem melakukan validasi login
- Bila sukses sistem akan mengarahkan ke halaman beranda
- Bila gagal sistem akan menampilkan peringatan

### 2.2.2  Admin Menginputkan Data
Use Case Admin Menginputkan Data

![alt text](2.2.2.png)

Diagram : 
Deskripsi Staf Dinas dapat melihat semua data yang ada didalam sistem . Deskripsi Langkah-langkah
- Kepala desa mengklik navbar laporan
- Kepala desa memilih combobox tersebut dan klik tombol cetak laporan
- Sistem akan menampilkan hasil laporan.

### 2.2.3 Admin View
Use Case Admin View 

![alt text](2.2.3.png)

Diagram : 
Deskripsi Admin dapat melihat semua data yang ada didalam sistem. 
Deskripsi Langkah-langkah
- Kepala desa mengklik navbar laporan
- Kepala desa memilih combobox tersebut dan klik tombol cetak laporan
- Sistem akan menampilkan hasil laporan.


## 2.3  Spesifikasi Kebutuhan Non Fungsional
- Tabel kebutuhan Non fungsional

![alt text](2.3SpekNonFungsional.png)


## 2.4 Karakteristik Pengguna

Karakteristik pengguna sistem konservasi bersama masyarakat ini adalah pengguna langsung berinteraksi dengan sistem tanpa harus dihubungkan dengan hak akses atau level autentikasi.


## 2.5  Batasan-batasan

- Perangkat lunak hanya bisa di jalankan pada windows (7,8,10,11)
- Waktu pengembangan perangkat lunak ini singkat dan kemungkinan tidak semua fungsi dapat dijalankan pada pengembangan sistem ini.


## 2.6  Asumsi- asumi

Maksimal penginputan id atau memasukkan kode pada aplikasi ini adalah 9999, lebih dari itu program akan muncul peringatan"Anda telah melebihi batas maksimum".


## 2.7 Kebutuhan Penyeimbang

Tidak ada


# BAB III Requirement Specification

## 3.1 Persyaratan Antarmuka Eksternal

Salah satu cara mengakses aplikasi ini yaitu dengan hak akses yang diberikan oleh admin, login melalui aplikasi ini dengan mencantumkan username kemudian sistem akan mencocokkan username Staf Dinas dan Staf Desa. Setelah login berhasil Staf Dinas dapat melihat semua data yang ada dalam sistem.

## 3.2 Functional Requirement

Logika Struktur terdapat pada bagian 3.3.1

### 3.2.1 Admin login
![alt text](3.2.1.png)

### 3.2.2 Admin input data 
![alt text](3.2.2.png)

### 3.2.3 Admin View data
![alt text](3.2.3.png)


### 3.3  Struktur Detail Kebutuhan Non- Fungsional

### 3.3.1 Logika Struktur Data
Struktur data logika pada sistem Aplikasi presensi menggunakan kehadiran terdapat struktur Database yang dijelaskan menggunakan ERD.
![alt text](ERD.png)

## Table Konservasi

![alt text](tablekonservasi.png)

## JOBDESK PER ANGGOTA
JASON WONG :  
- Buat ERD 
- Buat Tabel
- Buat Bab 3 
- Buat dan Upload Gitlab

FITRI IRMA YANTI :  
- Buat Bab 2
- Buat Bab 3
- Use Case

RENDI ANDRIYAN CORNALIUS SIMARMATA :  
- Buat Bab 1
- Tampilan Figma

